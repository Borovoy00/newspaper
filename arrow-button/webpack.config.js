const path = require('path');
MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: {
    plugToHtml: [
      "./src/index.js",
    ],
  },
  module: {
    rules: [
      {
        test: /.js/,
        enforce: 'pre',
        exclude: /node_modules/,
        use: [
          {
            loader: `babel-loader`,
          }
        ]
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"]
      }
    ]
  },
  resolve: {
    extensions: [".js", ".css", ".ts", ".scss"]
  },
  output: {
    filename: 'plugin.js',
    path: path.resolve(__dirname, './'),
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "styles.css",
    })
  ]
}
