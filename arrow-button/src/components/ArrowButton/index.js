import React from 'react';

import IconButton from '@material-ui/core/IconButton';

import ArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import ArrowRight from '@material-ui/icons/KeyboardArrowRight';

import { toggle } from '../../services';

import './style.css';

class ArrowButton extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      date: new Date(),
    };
  }

  componentDidMount(){
    window.addEventListener('date', e => this.setState({date: e.detail.date}));
    window.addEventListener("keydown", event => {
      if(event.keyCode == (this.props.reverse ? 39 : 37)) {
        toggle(this.props.reverse, this.state.date);
      }
    });
  }

  render() {
    return(
      <div className={this.props.reverse ? 'blockReverse' : 'block'}>
        <IconButton
          id='button'
          disableRipple
          size='medium'
          className='arrowButton'
          onClick={() => toggle(this.props.reverse, this.state.date)}
        >
          {this.props.reverse
            ? <div className='arrowBlock'> Next day <ArrowRight/></div>
            : <div className='arrowBlock'><ArrowLeft/> Previous day </div>}
        </IconButton>
      </div>
    )
  }
};

export default ArrowButton;
