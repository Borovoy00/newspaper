export function toggle(reverse, date){
  date.setDate(date.getDate() + (reverse? 1 : -1));
  window.dispatchEvent(new CustomEvent('date', { bubbles: true, detail: { date: date } }));
  document.scrollTo({top: 0, behavior: "smooth"});
}
