import React from 'react';

import ArrowButton from './components/ArrowButton';

import './App.css';

function App() {
  return (
    <div className="App">
      <ArrowButton/>
      <ArrowButton reverse/>
    </div>
  );
}

export default App;
