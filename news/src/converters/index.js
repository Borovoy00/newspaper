export const convertToShortDate = (date) => {
  return new Date(date).toLocaleDateString('en-GB');
}
