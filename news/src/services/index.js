export function getDate(){
  return localStorage.getItem('date');
}

export function getUrl(currentDate, currentTheme){
  let date = new Date(currentDate).toISOString().substring(0, 10);

  return `https://newsapi.org/v2/everything?q=${currentTheme ? currentTheme : 'trump'}&from=${date}&to=${date}&sortBy=popularity&apiKey=f8070ff4996141d6bf66c28410067768`;
}

export function scrolToElementById(id) {
  var element = document.getElementById(id);

  element.scrollIntoView({
    behavior: 'smooth',
    block: 'center',
    inline: 'center'
  });
}
