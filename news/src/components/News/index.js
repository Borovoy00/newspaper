import React from 'react';
import axios from 'axios';

import Button from '@material-ui/core/Button';

import { Article } from '../Article';
import { ButtonTopic } from '../ButtonTopic';
import ClipLoader from 'react-spinners/ClipLoader';

import { getUrl } from '../../services';
import { convertToShortDate } from '../../converters'
import { topics } from '../../constants';

import './style.css';

class News extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      date: new Date(),
      news: null,
      topic: 'trump',
    }
  }

  setNews(){
    axios.get(getUrl(this.state.date, this.state.topic))
      .then(response => {
        if(response.data.articles.length > 0) {
          this.setState({ news: response.data.articles })
        } else {
          this.setState({ news: null })
        }
      })
      .catch(error => this.setState({ news: null }))
  }

  listener(){
    window.addEventListener('date', e => {
      this.setState({ date: e.detail.date });
      this.setNews();
    });
  }

  componentDidMount(){
    this.setNews();
    this.listener();
  }

  handleChangeTopic(topic){
    this.setState({ topic },() => {
      this.setNews();
    });
  }

  render(){
    if(this.state.news) {
      return(
        <div className='news'>
          <div className='panel'>
            {topics.map(topic =>
              <ButtonTopic
                label={topic}
                onClick={() => this.handleChangeTopic(topic)}
                disabled={topic === this.state.topic}
                className='buttonTopic'
              >
                {topic}
              </ButtonTopic>
            )}
          </div>
          {this.state.news.map((article, index) =>
            <Article article={article} index={index}/>
          )}
        </div>
      )
    }

    return (
      <ClipLoader
        sizeUnit={"px"}
        size={150}
        color={'grey'}
      />
    );
  }
}

export default News;
