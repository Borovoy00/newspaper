import React from 'react';

import Button from '@material-ui/core/Button';

import './style.css';

export const ButtonTopic = (props) => {
  return (
    <Button
      className='buttonTopic'
      onClick={props.onClick}
      disabled={props.disabled}
      className={props.className}
    >
      {props.label}
    </Button>
  );
};
