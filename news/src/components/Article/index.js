import React from 'react';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import { convertToShortDate } from '../../converters';
import { scrolToElementById } from '../../services';

import './style.css';

export const Article = (props) => {
  return (
    <Card
      id={`cardMedia${props.index + 1}`}
      className='card'
      onClick={() => scrolToElementById(`cardMedia${props.index + 1}`)}
    >
      <CardHeader
        avatar={
          <Avatar className='avatar'>
            {props.article.title ? props.article.title.charAt(0) : '!'}
          </Avatar>
        }
        title={props.article.title}
        subheader={convertToShortDate(props.article.publishedAt)}
      />
      <CardMedia
        className='media'
        image={props.article.urlToImage}
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {props.article.description}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <Button
          className='readMore'
          href={props.article.url}
        >
          Read more
        </Button>
      </CardActions>
    </Card>
  );
}
