# NewsPaper

## Installation/Before starting

Create your prod build in apps.

## Calendar

Entry to ***calendar*** folder

```bash
cd calendar
```

Run build

```bash
npm run build
```

## News

Entry to ***news*** folder

```bash
cd news
```

Run build

```bash
npm run build
```

## ArrowButton

Entry to ***arrow-button*** folder

```bash
cd arrow-button
```

Run build

```bash
npm run build
```

