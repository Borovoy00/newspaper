import React from 'react';

import 'react-daypicker/lib/DayPicker.css';
import DayPicker from 'react-daypicker';

import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';
import Menu from '@material-ui/core/Menu';
import CalendarIcon from '@material-ui/icons/CalendarToday';

import { convertToLongDate } from '../../converters'

import './style.css';

function saveDate(date){
  window.dispatchEvent(new CustomEvent('date', { bubbles: true, detail: { date: date } }));
}

class Calendar extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      date: new Date(),
      anchor: null,
      openCalendar: false,
    }

    this.handleOpenClose = this.handleOpenClose.bind(this);
  }

  componentDidMount(){
    saveDate(new Date());
    this.listener();
  }

  listener(){
    window.addEventListener('date', e => {this.setState({ date: e.detail.date })})
  }

  handleOpenClose(e){
    this.setState({
      openCalendar: !this.state.openCalendar,
      anchor: e.currentTarget,
    });
  }

  render() {
    return(
      <div className='calendar'>
        <Typography variant="h3" gutterBottom className='dateText'>
          {convertToLongDate(this.state.date)}
          <Fab onClick={this.handleOpenClose}>
            <CalendarIcon/>
          </Fab>
        </Typography>
        <Menu
          open={this.state.openCalendar}
          onClose={this.handleOpenClose}
          anchorEl={this.state.anchor}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          <DayPicker
            style={{margin: 'auto'}}
            onDayClick={(day) => {
              saveDate(day);
            }}
          />
        </Menu>
      </div>
    )
  }
}

export default Calendar;
