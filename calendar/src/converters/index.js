export const convertToShortDate = (date) => {
  return new Date(date).toLocaleDateString('en-GB');
}

export const convertToLongDate = (date) => {
  return new Date(date).toLocaleString('en-GB', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  });
}
